# Frontend Challenge React

* A small webapp that will list the most starred Github repos.

## Instructions

Clone project: `git clone https://Omayma-Lemlih@bitbucket.org/Omayma-Lemlih/frontend-challenge-react.git`

Install node dependencies: `npm install`

Run project: `npm serve` for a dev server
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Resources

-[Bulma](https://bulma.io/)
-[Axios](https://www.npmjs.com/package/react-axios)
-[Moment.js](https://momentjs.com/)
