import React, { Component } from 'react';

class Navbar extends Component {
    render() {
        return (
            <div>
                <nav className="navbar is-dark" role="navigation" aria-label="main navigation">
                    <div className="navbar-brand">
                        <p className="navbar-item" href="#">
                            Coding Challenge
                        </p>
                    </div>
                </nav>
            </div>
        );
    }
}

export default Navbar;
