import React, { Component } from 'react';
import Repo from '../Repo/Repo';
import axios from 'axios';

class RepoList extends Component {

    state = {
        repos: []
    }

    componentDidMount() {
        this.getRepos();
    }

    getRepos = async (page = 1) => {
        console.log('Get Repose');
        try {
            const response = await axios.get(`https://api.github.com/search/repositories?q=created:>2017-10-22&sort=stars&order=desc`);
            this.setState({
                repos: response.data.items
            });
        } catch (error) {
            console.log(error);
        }
    }

    render() {

        return (
            <div>
                {
                    this.state.repos.map((item, index) => <Repo key={index} item={item} />)
                }
            </div>
        );
    }
}

export default RepoList;
