import React, { Component } from 'react';
import moment from 'moment';

class Repo extends Component {
    render() {
        return (
            <div className="media">
                <div className="media-left">
                    <p className="image is-64x64">
                        <img src={this.props.item.owner.avatar_url} />
                    </p>
                </div>
                <div className="media-content">
                    <div className="content">
                        <p>
                            <strong>{this.props.item.name}</strong> <small>By {this.props.item.owner.login}</small>. <small>Created {moment(this.props.item.created_at).fromNow()}</small>
                            <br />
                            {this.props.item.description}
                        </p>
                    </div>
                    <nav className="level is-mobile">
                        <div className="level-left">
                            <small className="level-item">
                                Stars:{this.props.item.stargazers_count}
                            </small>
                            <small className="level-item">
                                Issues:{this.props.item.open_issues}
                            </small>
                        </div>
                    </nav>
                </div>
            </div>
        );
    }
}

export default Repo;
