import React, { Component } from 'react';
import RepoList from './RepoList/RepoList';
import Navbar from './Navbar/Navbar';

class App extends Component {

  render() {
    return (
      <div>
        <Navbar />
        <br />
        <div className="container">
          <RepoList />
        </div>
      </div>
    );
  }
}

export default App;
